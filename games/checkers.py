import sys
sys.path.insert(0, '../src')

from Player import Player
from Piece import Piece
from Game import *
from Move import *

def requiredJumps(game, piece):
    if piece.name == "Regular":
        dy = -2 if piece.player is red else 2
        jumpLocations = [(piece.x - 2, piece.y + dy), (piece.x + 2, piece.y + dy)]
    else:
        jumpLocations = [ \
            (piece.x - 2, piece.y - 2), (piece.x - 2, piece.y + 2), \
            (piece.x + 2, piece.y - 2), (piece.x + 2, piece.y + 2) \
        ]

    jumpLocations = [ \
        loc for loc in jumpLocations if \
            0 <= loc[0] and loc[0] < game.width and \
            0 <= loc[1] and loc[1] < game.height \
        ]

    requiredMoves = set()
    for x, y in jumpLocations:
        if game.pieceAtLocation(x, y):
            continue

        midX = (x + piece.x) / 2
        midY = (y + piece.y) / 2

        enemy = game.enemy(piece.player)
        if game.playerPieceAtLocation(enemy, midX, midY):
            requiredMoves.add((piece, x, y))

    return requiredMoves

def computeRequiredMoves(game, player):
    requiredMoves = set()
    for piece in player.pieces:
        requiredMoves = requiredMoves | requiredJumps(game, piece)
    return requiredMoves

def jump(game, piece, x, y):
    if game.pieceAtLocation(x, y):
        return False

    midX = (x + piece.x) / 2
    midY = (y + piece.y) / 2
    res = game.enemyPieceAtLocation(midX, midY)

    if res:
        game.remove(midX, midY)

    return res

g = Game(8)

red = Player("Red", "R")
black = Player("Black", "B")

g.player(1, red)
g.player(2, black)

regular = Piece("Regular", ".")
regular.specialMove(nw(1), lambda game, piece, x, y: not game.pieceAtLocation(x, y))
regular.specialMove(ne(1), lambda game, piece, x, y: not game.pieceAtLocation(x, y))
regular.specialMove(nw(2), jump)
regular.specialMove(ne(2), jump)
regular.canJump()

kings = Piece("King", "K")
kings.specialMove(diagonally(1), lambda game, piece, x, y: not game.pieceAtLocation(x, y))
kings.specialMove(diagonally(2), jump)
kings.canJump()

def promote(game, piece, x, y):
    lastRow = 0 if piece.player is red else 7
    if piece.name == "Regular" and y == lastRow:
        game.remove(x, y)
        game.add(piece.player, kings, (x, y))

g.event(promote)

redLocations = []
for i in xrange(5, 8):
    for j in xrange(8):
        if (i % 2 == 1 and j % 2 == 0) or (i % 2 == 0 and j % 2 == 1):
            redLocations.append((j, i))

blackLocations = []
for i in xrange(3):
    for j in xrange(8):
        if (i % 2 == 1 and j % 2 == 0) or (i % 2 == 0 and j % 2 == 1):
            blackLocations.append((j, i))

# These commented out locations test (required) multi-jumps
# redLocations = [(2,6)]
# blackLocations = [(3,5), (3,3), (3,1), (6,0)]

g.addMultiple(red, regular, redLocations)
g.addMultiple(black, regular, blackLocations)

g.winCondition(lambda winner, loser : len(loser.pieces) == 0)
g.requiredMoves(computeRequiredMoves)

def switchPlayers(game, player):
    lastMove = game.getLastMove()
    if lastMove is None:
        return True

    lastPiece, oldX, oldY, newX, newY = lastMove
    if abs(oldX - newX) == 2 and abs(oldY - newY) == 2:
        return len(requiredJumps(game, lastPiece)) == 0
    return True

g.turn([GameActions.MOVE], switchPlayers)

g.start()