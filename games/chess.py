import sys
sys.path.insert(0, '../src')

from Player import Player
from Piece import Piece
from Game import *
from Move import *

g = Game(8)

white = Player("White", "W")
black = Player("Black", "B")

g.player(1, white)
g.player(2, black)

pawns = Piece("Pawn", "P")
# Normal movement
pawns.specialMove(up(1), lambda game, piece, x, y : not game.enemyPieceAtLocation(x, y))

# First turn movement
pawns.specialMove(up(2), lambda game, piece, x, y : not piece.hasMoved())

# Capture movement
pawns.specialMove(nw(1), lambda game, piece, x, y : game.enemyPieceAtLocation(x, y))
pawns.specialMove(ne(1), lambda game, piece, x, y : game.enemyPieceAtLocation(x, y))

def enPassant(game, piece, x, y):
    lastMove = game.getLastMove()
    if lastMove is None:
        return False

    lastPiece, oldX, oldY, newX, newY = lastMove
    return \
        lastPiece.getName() == "Pawn" and \
        abs(oldY - newY) == 2 and \
        piece.y == newY and \
        abs(piece.x - newX) == 1

def enPassantNW(game, piece, x, y):
    if not enPassant(game, piece, x, y):
        return False

    lastPiece, oldX, oldY, newX, newY = game.getLastMove()
    if game.currPlayer is white:
        if oldX == piece.x - 1:
            game.remove(newX, newY)
            return True
    else:
        if oldX == piece.x + 1:
            game.remove(newX, newY)
            return True
    return False

def enPassantNE(game, piece, x, y):
    if not enPassant(game, piece, x, y):
        return False

    lastPiece, oldX, oldY, newX, newY = game.getLastMove()
    if game.currPlayer is white:
        if oldX == piece.x + 1:
            game.remove(newX, newY)
            return True
    else:
        if oldX == piece.x - 1:
            game.remove(newX, newY)
            return True
    return False

pawns.specialMove(nw(1), enPassantNW)
pawns.specialMove(ne(1), enPassantNE)

def promote(game, piece, x, y):
    lastRow = 0 if piece.player is white else 7
    if piece.name == "Pawn" and y == lastRow:
        game.remove(x, y)

        while True:
            try:
                prompt = "Enter (case-sensitive) a piece to promote to from among [rook, knight, bishop, queen]"
                line = raw_input("\n[Player " + game.currPlayer.name + "] " + prompt + "\n")
            except (EOFError):
                print("No more input. Ending game...")
                sys.exit(1)

            if line == "rook":
                game.add(piece.player, rooks, (x, y))
                break
            elif line == "knight":
                game.add(piece.player, knights, (x, y))
                break
            elif line == "bishop":
                game.add(piece.player, bishops, (x, y))
                break
            elif line == "queen":
                game.add(piece.player, queens, (x, y))
                break

g.event(promote)

def kingSideCastle(game, piece, x, y):
    if "king side rook" not in game.currPlayer.specialPieces:
        return False

    rook = game.currPlayer.specialPieces["king side rook"]
    if piece.hasMoved() or rook.hasMoved():
        return False

    between = [(4,7), (5,7), (6,7)] if game.currPlayer is white else [(4,0), (5,0), (6,0)]
    for space in between:
        if game.enemyCanMoveToLocation(space[0], space[1]):
            return False

    rookTarget = between[1]
    game.teleport(rook, rookTarget[0], rookTarget[1])
    return True

def queenSideCastle(game, piece, x, y):
    if "queen side rook" not in game.currPlayer.specialPieces:
        return False

    rook = game.currPlayer.specialPieces["queen side rook"]
    if piece.hasMoved() or rook.hasMoved():
        return False

    blindSpot = (1,7) if game.currPlayer is white else (1,0)
    if game.pieceAtLocation(blindSpot[0], blindSpot[1]):
        return False

    between = [(2,7), (3,7), (4,7)] if game.currPlayer is white else [(2,0), (3,0), (4,0)]
    for space in between:
        if game.enemyCanMoveToLocation(space[0], space[1]):
            return False

    rookTarget = between[1]
    game.teleport(rook, rookTarget[0], rookTarget[1])
    return True

def inDanger(game, piece, x, y):
    enemy = game.enemy(piece.player)

    # Literally walking into check
    if game.playerCanMoveToLocation(enemy, x, y):
        return True

    # Trying to capture at a protected space
    if game.playerPieceAtLocation(enemy, x, y) and game.playerCanMoveToLocation(enemy, x, y, protect=True):
        return True

    return False

kings = Piece("King", "K")
kings.specialMove(horizontally(1), lambda game, piece, x, y : not inDanger(game, piece, x, y))
kings.specialMove(vertically(1), lambda game, piece, x, y : not inDanger(game, piece, x, y))
kings.specialMove(diagonally(1), lambda game, piece, x, y : not inDanger(game, piece, x, y))
kings.specialMove(dontFlip(right(2)), kingSideCastle)
kings.specialMove(dontFlip(left(2)), queenSideCastle)

queens = Piece("Queen", "Q")
queens.move(diagonally())
queens.move(horizontally())
queens.move(vertically())

rooks = Piece("Rook", "R")
rooks.move(horizontally())
rooks.move(vertically())

bishops = Piece("Bishop", "B")
bishops.move(diagonally())

knights = Piece("Knight", "N")
knights.move([vertically(2), horizontally(1)])
knights.move([horizontally(2), vertically(1)])
knights.canJump()

g.addMultiple(white, pawns, g.row(6))
g.add(white, rooks, (0,7), "queen side rook")
g.add(white, rooks, (7,7), "king side rook")
g.addMultiple(white, knights, [(1,7), (6,7)])
g.addMultiple(white, bishops, [(2,7), (5,7)])
g.add(white, kings, (4, 7), "king")
g.add(white, queens, (3, 7))

g.addMultiple(black, pawns, g.row(1))
g.add(black, rooks, (0,0), "queen side rook")
g.add(black, rooks, (7,0), "king side rook")
g.addMultiple(black, knights, [(1,0), (6,0)])
g.addMultiple(black, bishops, [(2,0), (5,0)])
g.add(black, kings, (4, 0), "king")
g.add(black, queens, (3, 0))

def checkmate(winner, loser):
    king = loser.specialPieces["king"]

    if not g.playerCanMoveToLocation(winner, king.x, king.y):
        return False

    if g.pieceCanMove(king):
        return False

    # Check if other pieces can capture or block checking piece
    listCopy = copy.copy(loser.pieces)
    for piece in listCopy:
        for x in xrange(g.width):
            for y in xrange(g.height):
                if g.canMove(loser, piece, (x, y)) and \
                not g.playerCanMoveToLocationAfter(winner, loser, king.x, king.y, piece, x, y):
                    return False
    return True

g.winCondition(checkmate)

g.turn([GameActions.MOVE])

g.start()
