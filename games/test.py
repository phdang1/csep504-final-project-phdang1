# Adapted from https://stackoverflow.com/questions/35777784/tkinter-button-coordinates-on-a-grid

import tkinter as tk
import sys

width = int(sys.argv[1])
height = int(sys.argv[2])

newLine = False

def click(row, col):
    global newLine
    print str(col) + " " + str(row),
    if newLine:
        print "\n",
        newLine = False
    else:
        newLine = True

root = tk.Tk()
for col in range(width):
    for row in range(height):
        button = tk.Button(root, text="%s,%s" % (col, row), height=5, width=10,
                           command=lambda row=row, col=col: click(row, col))
        button.grid(row=row, column=col, sticky="nsew")

root.mainloop()