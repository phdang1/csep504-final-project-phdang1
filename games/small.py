import sys
sys.path.insert(0, '../src')

from Player import Player
from Piece import Piece
from Game import Game, GameActions
from Move import *
from random import randint

# Define the board to be width 10, height 2
g = Game(10, 2)

# Define player "A" to have short name "A", and player "B" to have short name "B"
a = Player("A", "A")
b = Player("B", "B")

# Register to the game that player 1 is "A" and player 2 is "B"
g.player(1, a)
g.player(2, b)

# Define a type of Piece called "Marker", with short name "m"
# All "Marker" pieces will be printed on the board with the form "xm" where
# "x" is the short name for the Player who owns the "Marker" and "m" comes
# from the short name supplied here
marker = Piece("Marker", "m")

# Indicates that "Marker"s can only move right 1 space
# dontFlip prevents allowing the "Marker" to move left in the case of player 2
marker.move(dontFlip(right(1)))

# Instatiate a "Marker" for each player under the specialName "marker".
# Player "A"'s "Marker" starts at (3,0) while Player "B"'s "Marker" starts at (7,1)
g.add(a, marker, (3, 0), "marker")
g.add(b, marker, (7, 1), "marker")

# The game is over when the winner's piece is on the right most space of its row
# The piece is accessed by looking into the winner.specialPieces for the special
# name of that player's "Marker"
g.winCondition(lambda winner, loser : winner.specialPieces["marker"].x == 9)

# Each player makes one move per turn
g.turn([GameActions.MOVE])

# Starts the game
g.start()
