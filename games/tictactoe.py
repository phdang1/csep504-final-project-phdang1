import sys
sys.path.insert(0, '../src')

from Player import Player
from Piece import Piece
from Game import *

# Define the board to be width 3, and implicitly height = width = 3
g = Game(3)

# Define player "X" to have short name "X", and player "O" to have short name "O"
x = Player("X", "X")
o = Player("O", "O")

# Register to the game that player 1 is "X" and player 2 is "O"
g.player(1, x)
g.player(2, o)

# Define a type of piece "X" with short name "x"
xs = Piece("X", "x")

# Register that player "X" is allowed to place pieces of type "X"
x.place(xs)

# Define a type of piece "O" with short name "o"
os = Piece("O", "o")

# Register that player "O" is allowed to place pieces of type "O"
o.place(os)

# The game is won by winner if the board has 3 pieces in a row belonging to the winner
def f(winner, loser):
    return g.inARow(3, lambda pieces : allBelongsToPlayer(pieces, winner))

g.winCondition(f)

# The game is a draw (and is over) when the board is full
g.drawCondition(g.boardFull)

# Each player places one piece per turn
g.turn([GameActions.PLACE])

# Start the game
g.start()