class Player:
    def __init__(self, name, shortName):
        self.name = name
        self.shortName = shortName
        self.pieces = set()
        self.specialPieces = dict()
        self.placeablePieces = set()

    def place(self, piece):
        self.placeablePieces.add(piece)