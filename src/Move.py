import copy

class Direction:
    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3
    NW = 4
    NE = 5
    SW = 6
    SE = 7

class Move:
    def __init__(self, directions, n, cond=lambda game, piece, x, y: True, jump=False, flip=True):
        self.directions = directions
        self.n = n
        self.cond = cond
        self.jump = jump
        self.flip = flip

def up(n=float('inf')):
    return Move([Direction.UP], n)

def down(n=float('inf')):
    return Move([Direction.DOWN], n)

def right(n=float('inf')):
    return Move([Direction.RIGHT], n)

def left(n=float('inf')):
    return Move([Direction.LEFT], n)

def horizontally(n=float('inf')):
    return Move([Direction.LEFT, Direction.RIGHT], n)

def vertically(n=float('inf')):
    return Move([Direction.UP, Direction.DOWN], n)

def nw(n=float('inf')):
    return Move([Direction.NW], n)

def ne(n=float('inf')):
    return Move([Direction.NE], n)

def sw(n=float('inf')):
    return Move([Direction.SW], n)

def se(n=float('inf')):
    return Move([Direction.SE], n)

def diagonally(n=float('inf')):
    return Move([Direction.NW, Direction.NE, Direction.SW, Direction.SE], n)

def dontFlip(move):
    m = copy.deepcopy(move)
    m.flip = False
    return m

def oppositeDirectionMove(move):
    if type(move) is not list:
        if len(move.directions) > 1:
            return move

        if not move.flip:
            return move

        m = copy.deepcopy(move)
        if move.directions[0] == Direction.LEFT:
            m.directions[0] = Direction.RIGHT
        elif move.directions[0] == Direction.RIGHT:
            m.directions[0] = Direction.LEFT
        elif move.directions[0] == Direction.UP:
            m.directions[0] = Direction.DOWN
        elif move.directions[0] == Direction.DOWN:
            m.directions[0] = Direction.UP
        elif move.directions[0] == Direction.NW:
            m.directions[0] = Direction.SE
        elif move.directions[0] == Direction.NE:
            m.directions[0] = Direction.SW
        elif move.directions[0] == Direction.SW:
            m.directions[0] = Direction.NE
        elif move.directions[0] == Direction.SE:
            m.directions[0] = Direction.NW
        else:
            raise Exception("Direction not implemented yet")

        return m
    else:
        moves = []
        for m in move:
            moves.append(oppositeDirectionMove(m))
        return moves
