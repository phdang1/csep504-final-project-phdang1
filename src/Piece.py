class Piece:
    def __init__(self, name, shortName):
        self.name = name
        self.shortName = shortName
        self.player = None
        self.x = -1
        self.y = -1
        self.moves = []
        self.moved = False
        self.specialName = None

    def getName(self):
        return self.name

    def move(self, m):
        self.moves.append(m)

    def specialMove(self, m, cond):
        m.cond = cond
        self.move(m)

    def hasMoved(self):
        return self.moved

    def canJump(self):
        for move in self.moves:
            if type(move) is not list:
                move.jump = True
            else:
                for m in move:
                    m.jump = True
