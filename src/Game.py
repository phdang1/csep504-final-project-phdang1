from Player import Player
from Piece import Piece
from Move import *

import copy
import sys

class GameActions:
    MOVE = 0
    PLACE = 1

class Game:
    def __init__(self, width, height=None):
        self.width = width

        if height is not None:
            self.height = height
        else:
            self.height = width

        self.board = [[None for x in range(self.width)] for y in range(self.height)]

        self.players = [None, None]
        self.currPlayer = None
        self.drawConditionFunc = None
        self.lastMove = None
        self.events = []
        self.genRequiredMoves = lambda game, player : set()
        self.switchFn = lambda game, player : True

    def player(self, playerNum, player):
        self.players[playerNum - 1] = player

    def add(self, player, piece, location, specialName=None):
        p = copy.deepcopy(piece)
        p.player = player
        p.x = location[0]
        p.y = location[1]

        if specialName is not None:
            p.specialName = specialName
            player.specialPieces[specialName] = p

        # Second player gets the opposite direction of the first player
        # for single directional movements
        if player is self.players[1]:
            for i in xrange(len(p.moves)):
                p.moves[i] = oppositeDirectionMove(p.moves[i])

        player.pieces.add(p)
        self.board[p.y][p.x] = p

    def addMultiple(self, player, piece, locations):
        for location in locations:
            self.add(player, piece, location)

    def winCondition(self, f):
        def g():
            if f(self.players[0], self.players[1]):
                return self.players[0]
            elif f(self.players[1], self.players[0]):
                return self.players[1]
            else:
                return None
        self.winConditionFunc = g

    def drawCondition(self, f):
        self.drawConditionFunc = f

    def requiredMoves(self, f):
        self.genRequiredMoves = f

    def turn(self, actions, switchFn=lambda game, player : True):
        self.turnActions = actions
        self.switchFn =switchFn

    def playerNum(self, player):
        return 1 if self.players[0] is player else 2

    def start(self):
        if len(self.players) != 2:
            raise Exception("Not enough players")

        currPlayerNum = 0
        while True:
            winner = self.winConditionFunc()
            if winner is not None:
                self.printBoard()
                print("\n[Game over] Player " + str(self.playerNum(winner)) + " (" + winner.name + ") is the winner!")
                break

            if self.drawConditionFunc is not None and self.drawConditionFunc():
                self.printBoard()
                print("\n[Game over] The game is a draw")
                break

            self.currPlayer = self.players[currPlayerNum]
            for action in self.turnActions:
                self.printBoard()
                if action == GameActions.MOVE:
                    self.move(self.currPlayer)
                elif action == GameActions.PLACE:
                    self.place(self.currPlayer)
                else:
                    raise Exception("Invalid action")

            if self.switchFn(self, self.currPlayer):
                currPlayerNum = (currPlayerNum + 1) % 2

    def parseMoveLine(self, line):
        # x y x' y'
        tokens = line.split()
        return (int(tokens[0]), int(tokens[1])), (int(tokens[2]), int(tokens[3]))

    def parsePlaceLine(self, line):
        # shortName x y
        tokens = line.split()
        return tokens[0], (int(tokens[1]), int(tokens[2]))

    def leftLocations(self, x, y, move):
        if move.n != float('inf') and move.jump:
            newX = x - move.n
            return set([(newX, y)]) if self.onBoard(newX, y) else set()

        locations = set()
        start = 0 if move.n == float('inf') else max(0, x - move.n)
        for i in xrange(start, x):
            locations.add((i, y))
        return locations

    def rightLocations(self, x, y, move):
        if move.n != float('inf') and move.jump:
            newX = x + move.n
            return set([(newX, y)]) if self.onBoard(newX, y) else set()

        locations = set()
        end = self.width if move.n == float('inf') else min(x + move.n + 1, self.width)
        for i in xrange(x + 1, end):
            locations.add((i, y))
        return locations

    def upLocations(self, x, y, move):
        if move.n != float('inf') and move.jump:
            newY = y - move.n
            return set([(x, newY)]) if self.onBoard(x, newY) else set()

        locations = set()
        start = 0 if move.n == float('inf') else max(0, y - move.n)
        for i in xrange(start, y):
            locations.add((x, i))
        return locations

    def downLocations(self, x, y, move):
        if move.n != float('inf') and move.jump:
            newY = y + move.n
            return set([(x, newY)]) if self.onBoard(x, newY) else set()

        locations = set()
        end = self.height if move.n == float('inf') else min(y + move.n + 1, self.height)
        for i in xrange(y + 1, end):
            locations.add((x, i))
        return locations

    def nwLocations(self, x, y, move):
        if move.n != float('inf') and move.jump:
            newX = x - move.n
            newY = y - move.n
            return set([(newX, newY)]) if self.onBoard(newX, newY) else set()

        locations = set()
        curr = (x, y)
        i = 0
        while True:
            curr = (curr[0]-1, curr[1]-1)
            if curr[0] < 0 or curr[1] < 0 or i == move.n:
                return locations
            locations.add(curr)
            i += 1

    def neLocations(self, x, y, move):
        if move.n != float('inf') and move.jump:
            newX = x + move.n
            newY = y - move.n
            return set([(newX, newY)]) if self.onBoard(newX, newY) else set()

        locations = set()
        curr = (x, y)
        i = 0
        while True:
            curr = (curr[0]+1, curr[1]-1)
            if curr[0] >= self.width or curr[1] < 0 or i == move.n:
                return locations
            locations.add(curr)
            i += 1

    def swLocations(self, x, y, move):
        if move.n != float('inf') and move.jump:
            newX = x - move.n
            newY = y + move.n
            return set([(newX, newY)]) if self.onBoard(newX, newY) else set()

        locations = set()
        curr = (x, y)
        i = 0
        while True:
            curr = (curr[0]-1, curr[1]+1)
            if curr[0] < 0 or curr[1] >= self.height or i == move.n:
                return locations
            locations.add(curr)
            i += 1

    def seLocations(self, x, y, move):
        if move.n != float('inf') and move.jump:
            newX = x + move.n
            newY = y + move.n
            return set([(newX, newY)]) if self.onBoard(newX, newY) else set()

        locations = set()
        curr = (x, y)
        i = 0
        while True:
            curr = (curr[0]+1, curr[1]+1)
            if curr[0] >= self.width or curr[1] >= self.height or i == move.n:
                return locations
            locations.add(curr)
            i += 1

    def locationsFromMove(self, x, y, move):
        locations = set()
        for direction in move.directions:
            if direction == Direction.LEFT:
                locations = locations | self.leftLocations(x, y, move)
            elif direction == Direction.RIGHT:
                locations = locations | self.rightLocations(x, y, move)
            elif direction == Direction.UP:
                locations = locations | self.upLocations(x, y, move)
            elif direction == Direction.DOWN:
                locations = locations | self.downLocations(x, y, move)
            elif direction == Direction.NW:
                locations = locations | self.nwLocations(x, y, move)
            elif direction == Direction.NE:
                locations = locations | self.neLocations(x, y, move)
            elif direction == Direction.SW:
                locations = locations | self.swLocations(x, y, move)
            elif direction == Direction.SE:
                locations = locations | self.seLocations(x, y, move)
            else:
                raise Exception("Direction not implemented yet")
        return locations

    def potentialLocations(self, piece):
        locations = set()
        for move in piece.moves:
            locations = locations | self.locationsFromMove(piece.x, piece.y, move)
        return locations

    def pieceCanMove(self, piece):
        locations = self.potentialLocations(piece)
        for location in locations:
            if self.canMove(piece.player, piece, location):
                return True
        return False

    def canMoveLeft(self, piece, move, location):
        locations = self.leftLocations(piece.x, piece.y, move)

        if location not in locations:
            return False

        if move.n != float('inf') and not move.jump and location != (piece.x - move.n, piece.y):
            return False

        for loc in locations:
            if loc[0] > location[0] and self.board[loc[1]][loc[0]] is not None:
                return False
        return True

    def canMoveRight(self, piece, move, location):
        locations = self.rightLocations(piece.x, piece.y, move)

        if location not in locations:
            return False

        if move.n != float('inf') and not move.jump and location != (piece.x + move.n, piece.y):
            return False

        for loc in locations:
            if loc[0] < location[0] and self.board[loc[1]][loc[0]] is not None:
                return False
        return True

    def canMoveUp(self, piece, move, location):
        locations = self.upLocations(piece.x, piece.y, move)

        if location not in locations:
            return False

        if move.n != float('inf') and not move.jump and location != (piece.x, piece.y - move.n):
            return False

        for loc in locations:
            if loc[1] > location[1] and self.board[loc[1]][loc[0]] is not None:
                return False
        return True

    def canMoveDown(self, piece, move, location):
        locations = self.downLocations(piece.x, piece.y, move)

        if location not in locations:
            return False

        if move.n != float('inf') and not move.jump and location != (piece.x, piece.y + move.n):
            return False

        for loc in locations:
            if loc[1] < location[1] and self.board[loc[1]][loc[0]] is not None:
                return False
        return True

    def canMoveNW(self, piece, move, location):
        locations = self.nwLocations(piece.x, piece.y, move)

        if location not in locations:
            return False

        if move.n != float('inf') and not move.jump and location != (piece.x - move.n, piece.y - move.n):
            return False

        for loc in locations:
            if loc[0] > location[0] and loc[1] > location[1] and self.board[loc[1]][loc[0]] is not None:
                return False
        return True

    def canMoveNE(self, piece, move, location):
        locations = self.neLocations(piece.x, piece.y, move)

        if location not in locations:
            return False

        if move.n != float('inf') and not move.jump and location != (piece.x + move.n, piece.y - move.n):
            return False

        for loc in locations:
            if loc[0] < location[0] and loc[1] > location[1] and self.board[loc[1]][loc[0]] is not None:
                return False
        return True

    def canMoveSW(self, piece, move, location):
        locations = self.swLocations(piece.x, piece.y, move)

        if location not in locations:
            return False

        if move.n != float('inf') and not move.jump and location != (piece.x - move.n, piece.y + move.n):
            return False

        for loc in locations:
            if loc[0] > location[0] and loc[1] < location[1] and self.board[loc[1]][loc[0]] is not None:
                return False
        return True

    def canMoveSE(self, piece, move, location):
        locations = self.seLocations(piece.x, piece.y, move)

        if location not in locations:
            return False

        if move.n != float('inf') and not move.jump and location != (piece.x + move.n, piece.y + move.n):
            return False

        for loc in locations:
            if loc[0] < location[0] and loc[1] < location[1] and self.board[loc[1]][loc[0]] is not None:
                return False
        return True

    def canMoveNoList(self, piece, move, location):
        for direction in move.directions:
            if direction == Direction.LEFT:
                if self.canMoveLeft(piece, move, location):
                    return move.cond(self, piece, location[0], location[1])
            elif direction == Direction.RIGHT:
                if self.canMoveRight(piece, move, location):
                    return move.cond(self, piece, location[0], location[1])
            elif direction == Direction.UP:
                if self.canMoveUp(piece, move, location):
                    return move.cond(self, piece, location[0], location[1])
            elif direction == Direction.DOWN:
                if self.canMoveDown(piece, move, location):
                    return move.cond(self, piece, location[0], location[1])
            elif direction == Direction.NW:
                if self.canMoveNW(piece, move, location):
                    return move.cond(self, piece, location[0], location[1])
            elif direction == Direction.NE:
                if self.canMoveNE(piece, move, location):
                    return move.cond(self, piece, location[0], location[1])
            elif direction == Direction.SW:
                if self.canMoveSW(piece, move, location):
                    return move.cond(self, piece, location[0], location[1])
            elif direction == Direction.SE:
                if self.canMoveSE(piece, move, location):
                    return move.cond(self, piece, location[0], location[1])
            else:
                raise Exception("Direction not implemented yet")
        return False

    def canMoveList(self, moves, currLoc, targetLoc):
        if len(moves) == 1:
            return targetLoc in self.locationsFromMove(currLoc[0], currLoc[1], moves[0])

        canMove = False
        newLocations = self.locationsFromMove(currLoc[0], currLoc[1], moves[0])
        for newLoc in newLocations:
            canMove = canMove or self.canMoveList(moves[1:], newLoc, targetLoc)
        return canMove

    def canMove(self, currPlayer, piece, location, protect=False):
        # Disallow moving to a space occupied by your own pieces
        p = self.board[location[1]][location[0]]
        if not protect and p is not None and p.player is currPlayer:
            return False

        for move in piece.moves: # For each movement rule
            if type(move) is not list:
                if self.canMoveNoList(piece, move, location):
                    return True
            else:
                if self.canMoveList(move, (piece.x, piece.y), location):
                    return True
        return False

    def allowedMove(self, currPlayer, piece, newLocation):
        requiredMoves = self.genRequiredMoves(self, currPlayer)
        if len(requiredMoves) > 0:
            for move in requiredMoves:
                if move[0] is piece and move[1] == newLocation[0] and move[2] == newLocation[1]:
                    return True
            return False
        return True

    def move(self, currPlayer):
        piece = None
        newLocation = None
        while True:
            try:
                line = raw_input("\n[Player " + currPlayer.name + "] Make a move\n")
            except (EOFError):
                print("No more input. Ending game...")
                sys.exit(1)

            try:
                oldLocation, newLocation = self.parseMoveLine(line)
            except:
                print("Parsing error")
                continue

            if oldLocation[0] < 0 or oldLocation[0] >= self.width or \
               oldLocation[1] < 0 or oldLocation[1] >= self.height:
                print("Piece outside board")
                continue

            if newLocation[0] < 0 or newLocation[0] >= self.width or \
               newLocation[1] < 0 or newLocation[1] >= self.height:
                print("Target location outside board")
                continue

            piece = self.board[oldLocation[1]][oldLocation[0]]

            if piece is None:
                print("No piece selected")
                continue

            if piece.player is not currPlayer:
                print("Not your piece")
                continue

            if not self.allowedMove(currPlayer, piece, newLocation):
                print("You are required to make a different move")
                continue

            if self.canMove(currPlayer, piece, newLocation):
                break
            else:
                print("Illegal move")
                continue

        self.lastMove = (piece, piece.x, piece.y, newLocation[0], newLocation[1])
        self.teleport(piece, newLocation[0], newLocation[1])

        for event in self.events:
            event(self, piece, piece.x, piece.y)

    def place(self, currPlayer):
        while True:
            try:
                line = raw_input("\n[Player " + currPlayer.name + "] Place a piece\n")
            except (EOFError):
                print("No more input. Ending game...")
                sys.exit(1)

            try:
                shortName, location = self.parsePlaceLine(line)
            except:
                print("Parsing error")
                continue

            if location[0] < 0 or location[0] >= self.width or \
               location[1] < 0 or location[1] >= self.height:
                print("Location outside board")
                continue

            piece = self.board[location[1]][location[0]]

            if piece is not None:
                print("Location already contains a piece")
                continue

            for placeablePiece in currPlayer.placeablePieces:
                if shortName == placeablePiece.shortName:
                    self.add(currPlayer, placeablePiece, location)
                    return

            print(shortName + " is not a placable piece for you")

    def event(self, f):
        self.events.append(f)

    def getLastMove(self):
        return self.lastMove

    def remove(self, x, y):
        piece = self.board[y][x]
        if piece is not None:
            player = piece.player
            player.pieces.remove(piece)
            key = None
            for k in player.specialPieces:
                if player.specialPieces[k] is piece:
                    key = k

            if key is not None:
                del player.specialPieces[key]
            self.board[y][x] = None
        return piece

    def restore(self, piece):
        self.board[piece.y][piece.x] = piece
        player = piece.player
        player.pieces.add(piece)
        if piece.specialName is not None:
            player.specialPieces[piece.specialName] = piece

    def enemy(self, player):
        return self.players[0] if player is self.players[1] else self.players[1]

    # Unconditionally move piece to the location, and removes
    # all existing pieces at that location
    #
    # USE WITH CAUTION
    def teleport(self, piece, x, y):
        self.board[piece.y][piece.x] = None
        piece.x = x
        piece.y = y
        piece.moved = True
        self.remove(piece.x, piece.y)
        self.board[piece.y][piece.x] = piece

    def printBoard(self):
        s = ""
        for i in xrange(self.height):
            s += "  "
            for j in xrange(3 * self.width + 1):
                s += "_"
            s += "\n"
            s += str(i) + " |"

            for j in xrange(self.width):
                piece = self.board[i][j]
                if piece is None:
                    s += "  "
                else:
                    if piece.player is not None:
                        s += piece.player.shortName
                        s += piece.shortName
                    else:
                        raise Exception("Piece without a player")
                
                s += "|"
            s += "\n"

        s +=  "  "
        for j in xrange(3 * self.width + 1):
            s += "_"

        s += "\n  "
        for i in xrange(self.width):
            s += " " + str(i) + " "
        print(s)

    ######################################################################
    ######################### Board query functions ######################
    ######################################################################
    def onBoard(self, x, y):
        return 0 <= x and x < self.width and 0 <= y and y < self.height

    def boardFull(self):
        for i in xrange(self.height):
            for j in xrange(self.width):
                if self.board[i][j] is None:
                    return False
        return True

    # Checks if the board contains n pieces in a row that satisfy the
    # given condition (a function from list of pieces -> boolean)
    def inARow(self, n, cond):
        # rows
        for i in xrange(self.height):
            for j in xrange(self.width - n + 1):
                lst = []
                for k in xrange(n):
                    lst.append(self.board[i][j+k])
                if cond(lst):
                    return True
        # columns
        for i in xrange(self.height - n + 1):
            for j in xrange(self.width):
                lst = []
                for k in xrange(n):
                    lst.append(self.board[i+k][j])
                if cond(lst):
                    return True
        # diagonals
        for i in xrange(self.height - n + 1):
            for j in xrange(self.width - n + 1):
                lst = []
                for k in xrange(n):
                    lst.append(self.board[i+k][j+k])
                if cond(lst):
                    return True
        return False

    # Precondition: 0 <= n < self.height
    def row(self, n):
        if n < 0 or n >= self.height:
            raise Exception("Row not inside board dimensions")

        r = []
        for i in xrange(self.width):
            r.append((i, n))
        return r

    def pieceAtLocation(self, x, y):
        return self.board[y][x] is not None

    def playerPieceAtLocation(self, player, x, y):
        return self.board[y][x] is not None and self.board[y][x].player is player

    def enemyPieceAtLocation(self, x, y):
        p = self.board[y][x]
        if p is None:
            return False
        return p.player is not self.currPlayer

    def enemyCanMoveToLocation(self, x, y, protect=False):
        enemy = self.players[1] if self.currPlayer is self.players[0] else self.players[0]
        for piece in enemy.pieces:
            if self.canMove(enemy, piece, (x,y), protect):
                return True
        return False

    def playerCanMoveToLocation(self, player, x, y, protect=False):
        for piece in player.pieces:
            if self.canMove(player, piece, (x,y), protect):
                return True
        return False

    def playerCanMoveToLocationAfter(self, player, enemy, x, y, piece, afterX, afterY):
        oldPiece = self.remove(afterX, afterY)

        pieceX = piece.x
        pieceY = piece.y
        self.teleport(piece, afterX, afterY)

        res = self.playerCanMoveToLocation(player, x, y)

        self.teleport(piece, pieceX, pieceY)

        if oldPiece is not None:
            self.restore(oldPiece)

        return res

def allBelongsToPlayer(pieces, player):
    for p in pieces:
        if p is None or p.player is not player:
            return False
    return True

def printCoordinate(x, y):
    print("(" + str(x) + ", " + str(y) + ")")
